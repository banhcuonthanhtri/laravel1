<?php 
namespace App\Helpers;
use Config;

class Template {
    public static function showAreaSearch($controllerName,$paramSearch){
        $tmplField = Config::get('zvn.template.search');
        $fieldInController = Config::get('zvn.config.search');
     
        $controllerName = array_key_exists($controllerName,$fieldInController) ? $controllerName : 'default';
        $xhtmlField = null;
        $searchField = (in_array($paramSearch['field'],$fieldInController[$controllerName]))?$paramSearch['field']:"all";
      
        foreach($fieldInController[$controllerName] as $field) {//all id
            $xhtmlField .=sprintf(' <li><a href="#"
            class="select-field" data-field="%s">%s</a></li>',$field, $tmplField[$field]['name']);
        }
        $xhtml = sprintf('<div class="input-group">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle btn-active-field" data-toggle="dropdown" aria-expanded="false">
                                    %s<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    %s
                                </ul>
                            </div>
                            <input type="text" class="form-control" name="search_value" value="%s">
                            <span class="input-group-btn">
                        <button id="btn-clear" type="button" class="btn btn-success"
                                style="margin-right: 0px">Xóa tìm kiếm</button>
                        <button id="btn-search" type="button" class="btn btn-primary">Tìm kiếm</button>
                        </span>
                            <input type="hidden" name="search_field" value="%s">
                        </div>',$tmplField[$searchField]['name'],$xhtmlField,$paramSearch['value'],$searchField);
        return $xhtml;
    }
    public static function showButtonFilter($controllerName,$itemsStatusCount,$currentFilterStatus){//$currentFilterStatus
        $xhtml = null;
        $tmplStatus = Config::get('zvn.template.status', 'default');
        
        if(count($itemsStatusCount)>0){
            array_unshift($itemsStatusCount, [
                'count'=> array_sum(array_column($itemsStatusCount,'count')),
                'status'=> 'all'
            ]);
        
            foreach($itemsStatusCount as $item){
                $statusValue = $item['status'];
                $statusValue = array_key_exists($statusValue,$tmplStatus) ? $statusValue : 'default';
                $currentTemplateStatus = $tmplStatus[$statusValue];
                $link  =  route($controllerName)."?filter_status=".$statusValue;
                $class =  ($currentFilterStatus == $statusValue)? "btn-danger":"btn-info";
                $xhtml .= sprintf(
                    '<a href="%s" type="button" class="btn %s">%s <span class="badge bg-white">%s</span></a>',$link , $class,$currentTemplateStatus['name'],$item['count']);
            }
        }
        return $xhtml;
    }

    public static function ShowItemHistory($by,$time){
        $xhtml = sprintf(
        '<p><i class="fa fa-user"></i>%s</p>
        <p><i class="fa fa-clock-o"></i>%s</p>',$by,date(Config::get('zvn.format.long_time'),strtoTime($time)));
        return $xhtml;
    }

    public static function ShowItemStatus($controllerName,$id ,$status){
        $tmplStatus = Config::get('zvn.template.status', 'default');
        $statusValue = array_key_exists($status,$tmplStatus) ? $status : 'default';
        $currentTemplateStatus = $tmplStatus[$statusValue];
        $link = route($controllerName.'/status',['id'=>$id,'status'=>$status]);
        $xhtml = sprintf(
            '<a href="%s" type="button" class="btn btn-round %s">%s</a>',$status,$currentTemplateStatus ['class'],$currentTemplateStatus ['name']);
        return $xhtml;
    }

    public static function ShowItemThumb($controllerName,$thumbName, $thumbAlt){
        $xhtml = sprintf(
            '<img src="$s" alt="$s" class="zvn-thumb">',asset("images/$controllerName/$thumbName"),$thumbAlt);
        return $xhtml;
    }

    public static function showButtonAction($controllerName, $id){
        $tmplButton =  $fieldInController = Config::get('zvn.config.button');
        $buttonInArea = [
            'default'=>['edit','delete'],
            'slider' => ['info','edit','delete']
        ];
        $controllerName = (array_key_exists($controllerName,$buttonInArea))? $controllerName:"default";
        $listButtons = $buttonInArea[$controllerName];
         $xhtml = ' <div class="zvn-box-btn-filter">';
        foreach($listButtons as $btn){
            $currentButton = $tmplButton[$btn];
            $link = route( $controllerName.$currentButton['route-name'],['id'=>$id]);
            $xhtml .= sprintf(
                '<a href="%s" type="button" class="btn btn-icon %s" data-toggle="tooltip" data-placement="top" data-original-title="%s">
                <i class="fa %s"></i>
                </a>',$link ,$currentButton['class'],$currentButton['title'],$currentButton['icon']);
        }
       
        $xhtml .= ' </div>';
        return $xhtml;
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class SliderModel extends Model
{
    protected $table = 'slider';
    public $timestamps = false;
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    protected $filedSearchAccepted = [
        'id',
        'name',
        'description',
        'link'
    ];

    public function listItem($params,$options){
        $result = null;
        if($options['task']=="admin-list-items"){
            $query = $this->select('id','name','description','status','link','thumb','created','created_by','modified','modified_by');                 
            if($params['filter']['status']!="all"){
                $query ->where('status','=',$params['filter']['status']);
            } 
            if($params['search']['value']!=""){
                if($params['search']['field']=="all"){
                    $query->where(function($query) use ($params){
                        foreach($this->filedSearchAccepted as $column){
                            $query->orWhere($column,'LIKE',"%{$params['search']['value']}%");
                        }
                    });
                } else if(in_array($params['search']['field'],$this->filedSearchAccepted)){
                    $query->where($params['search']['field'],'LIKE',"%{$params['search']['value']}%");
                }
            }
            $result = $query ->orderBy('id','desc')
                    ->paginate($params['pagination']['totalItemPerPage']);                  
        }
        return $result;
    }

    public function countItems($params=null,$options=null){
        if($options['task']=="admin-count-items-group-by-status"){
            $result = self::select(DB::raw('count(id) as count,status'))
                        ->groupBy('status')
                        ->get()
                        ->toArray();
        }
        return $result;
    }
}
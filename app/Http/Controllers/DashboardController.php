<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class DashboardController extends Controller
{
    private $pathViewController = 'admin.dashboard.';
    private $controllerName = 'dashboard';
    
    public function index()
    {
        return view($this->pathViewController.'index', 
        ['controllerName'=>$this->controllerName ]);
    }

}

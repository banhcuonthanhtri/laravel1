<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class CategoryController extends Controller
{
    private $pathViewController = 'admin.category.';
    private $controllerName = 'category';
    
    public function __construct(){

        view()->share('controllerName', $this->controllerName);
    }

    public function index()
    {
        return view($this->pathViewController.'index');
    }

    public function form($id=0){
        $title = "SliderController - form";
        return view($this->pathViewController.'form',
        ['id'=>$id,'title'=>$title]);
    }

    public function delete(){
        return "Delete";
    }

    public function status(Request $request){
     
        echo '<h3>'.$request->route('id').'</h3>';
        echo '<h3>'.$request->route('status').'</h3>';
        echo '<h3>'.$request->id.'</h3>';
        echo '<h3>'.$request->status.'</h3>';
        return redirect()->route('slider');
    }
}

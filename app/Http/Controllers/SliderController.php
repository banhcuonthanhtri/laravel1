<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SliderModel as MainModel;
use DB;

class SliderController extends Controller
{
    private $pathViewController = 'admin.pages.slider.';
    private $controllerName = 'slider';
    private $params = [];
    private $model;

    public function __construct(){
        $this->model = new MainModel();
        $this->params["pagination"]['totalItemPerPage'] =3;
        view()->share('controllerName',$this->controllerName);
    }


    public function index(Request $request)
    {
       
        $mainModel = new MainModel();
        $this->params['filter']['status'] = $request->input('filter_status','all');//Trả ra mặt định là all
        $this->params['search']['field'] = $request->input('search_field','');
        $this->params['search']['value'] = $request->input('search_value','');
       
        $items              = $this->model->listItem($this->params,['task'=>'admin-list-items']);
        $itemsStatusCount   = $this->model->countItems($this->params,['task'=>'admin-count-items-group-by-status']);
       
        return view($this->pathViewController.'index', 
            [   'params'           => $this->params,
                'items'            => $items,
                'itemsStatusCount' => $itemsStatusCount
            ]);
    }

    public function form($id=0){
        $title = "SliderController - form";
        return view($this->pathViewController.'form',
        ['id'=>$id,'title'=>$title]);
    }

    public function delete(){
        return "Delete";
    }

    public function status(Request $request){
     
        echo '<h3>'.$request->route('id').'</h3>';
        echo '<h3>'.$request->route('status').'</h3>';
        echo '<h3>'.$request->id.'</h3>';
        echo '<h3>'.$request->status.'</h3>';
        return redirect()->route('slider');
    }
}

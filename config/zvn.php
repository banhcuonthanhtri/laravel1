<?php 

return [
   'url'=>[
        'prefix_admin'=>'quan-tri',
   ],
   'format' => [
      'long_time' => 'H:m:s d/m/Y',
      'short_time'=> 'd/m/Y'
   ],
   'template'=> [
      'status' => [
         'all' => ['name'=>'Tất cả phần tử','class' => 'btn-success'],
         'active' => ['name'=>'Kích hoạt','class' => 'btn-success'],
         'inactive' => ['name'=>'Chưa Kích hoạt','class' => 'btn-info'],
         'default' => ['name'=>'Không xác định','class' => 'btn-info']
      ],
      'search' => [
         'all' => ['name'=>'Search by ALL'],
         'id' => ['name'=>'Search by Id'],
         'name' => ['name'=>'Search by Username'],
         'fullname' => ['name'=>'Search by fullName'],
         'email' => ['name'=>'Search by Email'],
         'description' => ['name'=>'Search by Description'],
         'link' => ['name'=>'Search by Link'],
         'content' => ['name'=> 'Search by Content']
      ],
     
   ],
   'config' => [
      'search' => [
         'default' => ['all','id','fullname'],
         'slider'  => ['all','id','name','description','link']
      ],
      'button' => [
         'edit'=> ['class'=>'btn-success','title'=>'Edit','icon'=>'fa-pencil','route-name'=>'/form'],
         'delete'=> ['class'=>'btn-danger','title'=>'Delete','icon'=>'fa-trash','route-name'=>'/delete'],
         'info' => ['class'=>'btn-info','title'=>'View','icon'=>'fa-eye','route-name'=>'/delete'],
      ]
 

   ]
   
];

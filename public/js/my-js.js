$(document).ready(function(){
    let btnSearch = $('button#btn-search');
    let btnClearSearch = $('button#btn-searchclear');

    let $inputSearchField = $('input[name = search_field]');
    let $inputSearchValue = $('input[name = search_value]');

    $("a.search-field").click(function(e){
        e.preventDefault();
        let field = $(this).data('field');
        let fieldName = $(this).html();
        $("button.btn-active-field").html(fieldName + '<span class="caret"></span>');
        $inputSearchField.val(field);
    });

    btnSearch.click(function(){
        var pathname = window.location.pathname;
        let params = ['filter_status'];
        let searchParams = new URLSearchParams(window.location.search); // ?filter_status = active

        $.each(params,function(key,param){  // filter_Status
            if(searchParams.has(param)){
                link += param + "=" + searchParams.get(param)+ "&" //"filerter_status= inactive
            }
        });

        let search_field = $inputSearchField.val();
        let search_value = $inputSearchValue.val();
        if(search_value.replace(/\s/g,"")){
            alert("Moi nhap thong tin tim kiem");
        }
        window.location.href = pathname+"?"+ link +'search_field='+search_field+'&search_value='+search_value;
    });
})
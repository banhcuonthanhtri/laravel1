<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/about', function(){
    return "about";
});


Route::get('Category/{id?}', function ($id=1) {
    return 'Category '.$id;
})->where('id', '[0-9]+');


/*Route::prefix($prefixAdmin)->group(function(){

    Route::get('users', function() {
        return "admin/user";
    });
    Route::get('slider', function() {
        //
        return "admin/slider";
    });
    
});*/

$prefixAdmin = Config::get('zvn.url.prefix_admin', 'default');


/*Route::group(['prefix' =>$prefixAdmin], function() {
    //
    Route::get('users', function() {
        return "admin/user";
    });
    
    Route::group(['prefix' => 'slider'], function() {
        //
        
        Route::get('create', function() {
            //
            return "create";
        });

        
        Route::get('edit/{id?}', function($id=0) {
            //
            return "edit".$id;
        })->where('id','[0-9]+');
        
        
        Route::get('delete/{id?}', function($id=0) {
            //
            return "delete".$id;
        });
        
        
    });
    
    
});*/


Route::group(['prefix' => 'quan-tri'], function() {
    $prefix = 'slider';
    $controllerName = 'slider';
    Route::group(['prefix' => $prefix], function() use($controllerName) {
        $controller = ucfirst($controllerName).'Controller@';
        Route::get('/'          ,['as'=> $controllerName,           'uses'=> $controller.'index']);
        Route::get('form/{id?}' ,['as'=> $controllerName.'/form',   'uses'=>$controller.'index'])->where('id','[0-9]+');
        Route::get('delete/{id}',['as'=> $controllerName.'/delete', 'uses'=>$controller.'index'])->where('id','[0-9]+');
        Route::get('change-status-{id}/{status}',['as'=> $controllerName.'/status','uses'=>$controller.'status'])->where('id','[0-9]+');
    });

   
});



Route::group(['prefix' => $prefixAdmin], function() {
    $prefix = 'category123';
    $controllerName = 'category';
    Route::group(['prefix' => $prefix], function() use($controllerName) {
        $controller = ucfirst($controllerName).'Controller@';
        Route::get('/'          ,['as'=> $controllerName,           'uses'=> $controller.'index']);
        Route::get('form/{id?}' ,['as'=> $controllerName.'/form',   'uses'=> $controller.'form'])->where('id','[0-9]+');
        Route::get('delete/{id}',['as'=> $controllerName.'/delete', 'uses'=>$controller.'delete'])->where('id','[0-9]+');
        Route::get('change-status-{id}/{status}',['as'=> $controllerName.'/status','uses'=>$controller.'status'])->where('id','[0-9]+');
    });

   
});


Route::group(['prefix' => $prefixAdmin], function() {
    $prefix = 'dashboard';
    $controllerName = 'dashboard';
    Route::group(['prefix' => $prefix], function() use($controllerName) {
        $controller = ucfirst($controllerName).'Controller@';
        Route::get('/'          ,['as'=> $controllerName,           'uses'=> $controller.'index']);
      
    }); 
});



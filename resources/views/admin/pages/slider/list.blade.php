@php
    use App\Helpers\Template as Templates;
@endphp

<div class="x_content">
    <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
                <tr class="headings">
                    <th class="column-title">#</th>
                    <th class="column-title">Slider Info</th>
                    <th class="column-title">Trạng thái</th>
                    <th class="column-title">Tạo mới</th>
                    <th class="column-title">Chỉnh sửa</th>
                    <th class="column-title">Hành động</th>
                </tr>
            </thead>
            <tbody>
                @if (count($items)>0)
                @foreach ($items as $key=>$val)
                    @php
                        $id = $val['id'];
                        $index = $key + 1;
                        $name = $val['name'];
                        $class = ($index%2==0)? "even":"odd";
                        $description = $val['description'];
                        $link = $val['link'];
                        $image =Templates::ShowItemThumb($controllerName,$val['thumb'],$val['name']);
                        $status =  Templates::ShowItemStatus($controllerName,$id,$val['status']);
                        $created = $val['created'];
                        $created_by = $val['created_by'];
                        $modified = $val['modified'];
                        $modified_by = $val['modified_by'];     
                        $createdHistory = Templates::ShowItemHistory($created_by,$created);
                        $modifiedHistory = Templates::ShowItemHistory($modified_by,$modified);
                        $listBtnAction = Templates::ShowButtonAction($controllerName,$id);
                    @endphp                                    
                        <tr class="{{ $class }} pointer">
                            <td>{{  $index }}</td>
                            <td width="40%">
                                <p><strong>Name:</strong>{{ $name }}</p>
                                <p><strong>Description:</strong>{{ $description }}</p>
                                <p><strong>Link:</strong> {{ $link }}</p>
                                <p><img src="{{ $image }}" alt="Ưu đãi học phí" class="zvn-thumb"></p>
                            </td>
                            <td>{!! $status !!}</td>
                            <td>
                               {!! $createdHistory !!}
                            </td>
                            <td>
                               {!! $modifiedHistory !!}
                            </td>
                            <td class="last">
                                <div class="zvn-box-btn-filter">
                                   {!! $listBtnAction !!}
                                 </div>
                            </td>
                        </tr>
                                            
                    
                @endforeach
                   
                        @else
                           @include('admin.templates.list_empty',['colspan'=>6]);
                        @endif
                    </tbody>
        </table>
    </div>
</div>
@php
    $totalItems = $items->total();
    $totalPages = $items->lastPage();
    $totalItemPerPage = $items->perPage();
@endphp

<div class="x_content">
    <div class="row">
        <div class="col-md-6">
            <p class="m-b-0">Số phần tử trên trang: <b></b> {{$totalItemPerPage}} <span
                    class="label label-success label-pagination">3 trang</span></p>
            <p class="m-b-0">Hiển thị<b> 1 </b> đến<b> 2</b> trên<b> 6</b> Phần tử</p>
        </div>
        <div class="col-md-6">
           {{$items->links('pagination.pagination_backend')}}
        </div>
    </div>
</div>
